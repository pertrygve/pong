package pong;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Wall implements GraphicObj, Polygon {

	private List<Point2Double> points;
	private final Color fillColor;
	private final Color edgeColor;
	private List<Vector2Double> normalLines;
	private List<Vector2Double> normalPoints;

	public Wall(List<Point2Double> points, Color fillColor, Color edgeColor) {
		this.points = points;
		this.fillColor = fillColor;
		this.edgeColor = edgeColor;
		normalLines = new ArrayList<Vector2Double>();
		normalPoints = new ArrayList<Vector2Double>();
		Point2Double firstNormal = new Point2Double(0, 0);
		Point2Double prevNormal = new Point2Double(0, 0);
		for (int i = 0; i < points.size(); i++) {
			Point2Double p1 = points.get(i);
			Point2Double p2 = points.get((i + 1) % points.size());
			Point2Double vector = Point2Double.vector(p1, p2);
			Point2Double pCenter = Point2Double.add(p1, vector.scalar(0.5));
			
			double vectorLenth = vector.length();
			Point2Double normal = new Point2Double(vector.getY() * NORMAL_LENGTH / vectorLenth,
					-vector.getX() * NORMAL_LENGTH / vectorLenth);
			normalLines.add(new Vector2Double(pCenter, Point2Double.add(pCenter, normal)));

			if (i == 0) {
				firstNormal = normal;
			} else {
				Point2Double pointNormal = Point2Double.add(normal, prevNormal).scalar(0.5);
				normalPoints.add(new Vector2Double(p1, Point2Double.add(p1, pointNormal)));
			}
			prevNormal = normal;
			if (i == points.size() - 1) {
				Point2Double pointNormal = Point2Double.add(normal, firstNormal).scalar(0.5);
				normalPoints.add(new Vector2Double(p2, Point2Double.add(p2, pointNormal)));

			}
		}

	}

	@Override
	public Color getFillColor() {
		return fillColor;
	}

	@Override
	public Color getEdgeColor() {
		return edgeColor;
	}

	public List<Point2Double> getPoints() {
		return points;
	}

	@Override
	public List<Point2Double> getGlobalPoints() {
		return points;
	}

	@Override
	public int[][] getAll() {
		int[][] array = new int[2][];
		array[0] = new int[points.size()];
		array[1] = new int[points.size()];
		for (int i = 0; i < points.size(); i++) {
			array[0][i] = (int) points.get(i).getX();
			array[1][i] = (int) points.get(i).getY();
		}
		return array;
	}

	@Override
	public int[][] getNormalArray() {
		int[][] array = new int[4][];
		for (int i = 0; i < array.length; i++) {
			array[i] = new int[normalLines.size() + normalPoints.size()];
		}
		for (int i = 0; i < normalLines.size(); i++) {
			array[0][i] = (int) normalLines.get(i).getPoint1().getX();
			array[1][i] = (int) normalLines.get(i).getPoint1().getY();
			array[2][i] = (int) normalLines.get(i).getPoint2().getX();
			array[3][i] = (int) normalLines.get(i).getPoint2().getY();
		}
		for (int i = 0; i < normalPoints.size(); i++) {
			array[0][i+normalLines.size()] = (int) normalPoints.get(i).getPoint1().getX();
			array[1][i+normalLines.size()] = (int) normalPoints.get(i).getPoint1().getY();
			array[2][i+normalLines.size()] = (int) normalPoints.get(i).getPoint2().getX();
			array[3][i+normalLines.size()] = (int) normalPoints.get(i).getPoint2().getY();
		}
		return array;

	}
	@Override
	public List<Vector2Double> getNormalLines() {
		return normalLines;
	}
	@Override
	public List<Vector2Double> getNormalPoints() {
		return normalPoints;
	}


}
