package pong;

/**
 * Class to find intersection between 2 points with vectors scaling vectors so
 * that they meet. Only made by factory method since 2 lines might be parallel
 * and not be crossing
 * 
 * @author Per Trygve
 * 
 */
public class IntersectLines {
	private final Point2Double point1;
	private final Point2Double vector1;
	private final Point2Double point2;
	private final Point2Double vector2;

	private final double scalar1;
	private final double scalar2;




	private IntersectLines(Point2Double point1, Point2Double vector1,
			double scalar1, Point2Double point2, Point2Double vector2,
			double scalar2) {
		this.point1 = point1;
		this.vector1 = vector1;
		this.scalar1 = scalar1;
		this.point2 = point2;
		this.vector2 = vector2;
		this.scalar2 = scalar2;

	}

	public Point2Double getPoint1() {
		return point1;
	}

	public Point2Double getVector1() {
		return vector1;
	}

	public Point2Double getPoint2() {
		return point2;
	}

	public Point2Double getVector2() {
		return vector2;
	}

	public double getScalar1() {
		return scalar1;
	}

	public double getScalar2() {
		return scalar2;
	}

	/**
	 * Makes IntersectLines object if lines intersect does not matter where they intersects
	 * 
	 * @param point1
	 * @param vector1
	 * @param point2
	 * @param vector2
	 * @return null if they are parallel or one vector have length 0
	 */
	public static IntersectLines makeIntersectLines(Point2Double point1,
			Point2Double vector1, Point2Double point2, Point2Double vector2) {

		double scalar1;
		double scalar2;
		// point1 +vector1* scalar1 == point2 +vector2* scalar2
		/*
		 * p1 + v1*s1 = p2 + v2*s2
		 * 
		 * v1*s1 - v2*s2 = p2 -p1
		 * 
		 * s1 = det(p2x-p1x,-v2x,p2y-p1y,v1y)/det(v1x,-v2x,v1y,-v2y) 
		 * s2 = det(v1x,p2x-p1x,v1y,p2y-p1y)/det(v1x,-v2x,v1y,-v2y)
		 */
		double det = determainant(vector1.getX(), -vector2.getX(), vector1.getY(),
				-vector2.getY());
		if (det == 0) {
			return null;
		}
		scalar1 = determainant(point2.getX() - point1.getX(), -vector2.getX(), point2.getY()
				- point1.getY(), -vector2.getY())
				/ det;
		scalar2 = determainant(vector1.getX(), point2.getX() - point1.getX(), vector1.getY(),
				point2.getY() - point1.getY()) / det;

		IntersectLines intersect = new IntersectLines(point1, vector1, scalar1,
				point2, vector2, scalar2);
		return intersect;
	}

	private static double determainant(double a11, double a12, double a21,
			double a22) {
		return a11 * a22 - a12 * a21;
	}

}
