package pong;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Game extends JPanel {
	private CollisionHandler collisionHandler;
	public final static int sizeX = 1000;
	public final static int sizeY = 800;
	public final static int offSet = 100;
	public final static int paddleX = 100;
	public final static int paddleY = 20;
	
	public Game() {
		double radius = 50;
		int ballCount = 10;
		double speedFactor = 0.5;

		List<Wall> walls = new ArrayList<Wall>();
		List<Point2Double> wallList = new ArrayList<Point2Double>();

		wallList.add(new Point2Double(offSet * 3 / 4, offSet));
		for (int i = 0; i < (sizeY - 2 * offSet) / offSet; i++) {
			wallList.add(new Point2Double(offSet * 3 / 4 + Math.random() * offSet / 2,
					offSet * (i + 1) + Math.random() * offSet));
		}
		wallList.add(new Point2Double(offSet * 3 / 4, sizeY - offSet));
		wallList.add(new Point2Double(offSet / 4, sizeY - offSet));
		wallList.add(new Point2Double(offSet / 4, offSet));
		walls.add(new Wall(wallList, Color.YELLOW, Color.BLACK));

		wallList = new ArrayList<Point2Double>();
		wallList.add(new Point2Double(sizeX+offSet/4, offSet));
		wallList.add(new Point2Double(sizeX+offSet/4, sizeY - offSet));
		wallList.add(new Point2Double(sizeX - offSet / 4, sizeY - offSet));
		for (int i = 0; i < (sizeY - 2 * offSet) / offSet; i++) {
			wallList.add(new Point2Double(sizeX - offSet * 5 / 4 + Math.random() * offSet / 2,
					sizeY - offSet * (i + 2) + Math.random() * offSet));
		}
		wallList.add(new Point2Double(sizeX - offSet / 4, offSet));
		walls.add(new Wall(wallList, Color.YELLOW, Color.BLACK));

		wallList = new ArrayList<Point2Double>();
		wallList.add(new Point2Double(offSet, offSet / 4));
		wallList.add(new Point2Double(sizeX - offSet, offSet / 4));
		wallList.add(new Point2Double(sizeX - offSet, offSet * 3 / 4));
		for (int i = 0; i < (sizeX - 2 * offSet) / offSet; i++) {
			wallList.add(new Point2Double(sizeX - offSet * (i + 1) - Math.random() * offSet,
					offSet * 3 / 4 + offSet * Math.random()));
		}
		wallList.add(new Point2Double(offSet, offSet * 3 / 4));
		walls.add(new Wall(wallList, Color.YELLOW, Color.BLACK));

		List<Paddle> paddles = new ArrayList<Paddle>();
		List<Point2Double> paddleList = new ArrayList<Point2Double>();

		paddleList.add(new Point2Double(paddleX, -paddleY));
		paddleList.add(new Point2Double(paddleX, paddleY));
		paddleList.add(new Point2Double(-paddleX, paddleY));
		paddleList.add(new Point2Double(-paddleX, -paddleY));

		Point2Double paddleCenter = new Point2Double(sizeX / 2, sizeY - offSet / 2);
		Point2Double paddleV = new Point2Double(2, 0);

		paddles.add(new Paddle(paddleCenter, paddleV, paddleList, Color.CYAN, Color.BLACK));

		paddleV = new Point2Double(-1, 0);
		paddleCenter = new Point2Double(sizeX / 2, sizeY);

		paddles.add(new Paddle(paddleCenter, paddleV, paddleList, Color.CYAN, Color.BLACK));

		paddleV = new Point2Double(0,-0.7);
		paddleCenter = new Point2Double(sizeX / 2, sizeY);
		
		paddleList = new ArrayList<Point2Double>();
		paddleList.add(new Point2Double(paddleY, -paddleX));
		paddleList.add(new Point2Double(paddleY, paddleX));
		paddleList.add(new Point2Double(-paddleY, paddleX));
		paddleList.add(new Point2Double(-paddleY, -paddleX));

	//	paddles.add(new Paddle(paddleCenter, paddleV, paddleList, Color.CYAN, Color.BLACK));
		
		collisionHandler = new CollisionHandler(paddles, walls);
		for (int i = 0; i < ballCount; i++) {
			boolean ballOK = false;
			do {
				Ball ball = new Ball(new Point2Double(offSet+(sizeX-2*offSet) * Math.random(), offSet+(sizeY-offSet*2) * Math.random()),
						new Point2Double(speedFactor*(Math.random() * 2 - 1), speedFactor*(Math.random() * 2 - 1)), radius, Color.RED, Color.BLACK);
				if (collisionHandler.addBall(ball)) {
					ballOK = true;
				}
			} while (!ballOK);
		}

	}

	private void move() {
		collisionHandler.moveBalls();
		for (Paddle paddle : collisionHandler.getPaddles()) {
			 paddle.movePaddle();
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		for (Ball ball : collisionHandler.getBalls()) {
			drawObject(ball, g2d);
		}

		for (Wall wall : collisionHandler.getWalls()) {
		//	drawObject(wall, g2d);
		}
		for (Paddle paddle : collisionHandler.getPaddles()) {
			drawObject(paddle, g2d);
		}
		g2d.setColor(Color.BLUE.darker());
		g2d.drawRect(offSet/4, offSet/4, sizeX, sizeY);
		g2d.setColor(Color.ORANGE);
		//Grid x axis
		for(int i = 1 ; i <(offSet/4+sizeX)/offSet; i++) {
			g2d.drawLine(offSet/4+offSet*i, offSet/4, offSet/4+offSet*i, sizeY+offSet/4);
		}
		g2d.setColor(Color.MAGENTA);
		// Grid y axis
		for(int i = 1 ; i <(offSet/4+sizeY)/offSet; i++) {
			g2d.drawLine(offSet/4, offSet/4+offSet*i, offSet/4+sizeX, offSet/4+offSet*i);
		}
	}
	private void drawLine(Point2Double start, Point2Double end, Color color, Graphics2D g2d) {
		g2d.setColor(color);
		g2d.drawLine((int)start.getX(), (int)start.getY(), (int)end.getX(), (int)end.getY());
	}

	private void drawObject(GraphicObj gObject, Graphics2D g2d) {
		int[][] array = gObject.getAll();
		g2d.setColor(gObject.getFillColor());
		g2d.fillPolygon(array[0], array[1], array[0].length);
		g2d.setColor(gObject.getEdgeColor());
		g2d.drawPolygon(array[0], array[1], array[0].length);

		array = gObject.getNormalArray();

		g2d.setColor(Color.BLUE);
		for (int i = 0; i < array[0].length; i++) {
			g2d.drawLine(array[0][i], array[1][i], array[2][i], array[3][i]);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		JFrame frame = new JFrame("Sample Frame");
		Game game = new Game();
		frame.add(game);
		frame.setSize(sizeX + offSet*3/4, sizeY + offSet);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		while (true) {
			game.move();
			game.repaint();
			Thread.sleep(10);
		}
	}
}