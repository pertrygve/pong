package pong;

import java.util.ArrayList;
import java.util.List;

public class CollisionHandler {

	private List<Ball> balls;
	private List<Paddle> paddles;
	private List<Wall> walls;

	public CollisionHandler(List<Paddle> paddles, List<Wall> walls) {
		this.paddles = paddles;
		this.walls = walls;
		balls = new ArrayList<Ball>();
	}

	public List<Ball> getBalls() {
		return balls;
	}

	public List<Paddle> getPaddles() {
		return paddles;
	}

	public List<Wall> getWalls() {
		return walls;
	}

	public boolean addBall(Ball ball) {
		for (Ball ballOld : balls) {
			if (ball.getPosition().distanceSQ(ballOld.getPosition()) < (ball.getRadius() + ballOld.getRadius())
					* (ball.getRadius() + ballOld.getRadius())) {
				return false;
			}

		}
		for (Wall wall : walls) {
			if (pointIsInsidePolygon(wall, ball.getPosition())) {
				return false;
			}
		}
		for (Wall wall : walls) {
			if (!doIntersect(wall, ball.getPosition(), ball.getRadius())) {
				return false;
			}
		}
		for (Paddle paddle : paddles) {
			if (pointIsInsidePolygon(paddle, ball.getPosition())) {
				return false;
			}
		}
		for (Paddle paddle : paddles) {
			if (!doIntersect(paddle, ball.getPosition(), ball.getRadius())) {
				return false;
			}
		}
		balls.add(ball);
		return true;
	}

	public void moveBalls() {

		double timeUsed = 0;
		do {
			timeUsed += moveBalls(1 - timeUsed);
		} while (timeUsed < 1);
	}
	
	

	private double moveBalls(double timeLeft) {
		List<BallCollision> collisions = new ArrayList<BallCollision>();
		collisions.addAll(getBallCollisions(possibleBallCollisions(timeLeft),timeLeft));
		
		collisions.addAll(getPolygonCollision(walls, timeLeft));
		collisions.addAll(getCornerCollision(walls, timeLeft));
		collisions.addAll(getPolygonCollision(paddles, timeLeft));
		collisions.addAll(getCornerCollision(paddles, timeLeft));
		double firstEvent = timeLeft;
		for (BallCollision collision : collisions) {
			if (collision.getTime() < firstEvent) {
				firstEvent = collision.getTime();
			}
		}
		for (int i = 0; i < collisions.size(); i++) {
			if (collisions.get(i).getTime() != firstEvent) {
				collisions.remove(i);
				i--;
			}
		}
		for (Ball ball : balls) {
			BallCollision collisionFound = null;
			for (BallCollision collision : collisions) {
				if (collision.getBall().equals(ball)) {
					collisionFound = collision;
					break;
				}
			}
			if (collisionFound == null) {
				ball.movePartial(firstEvent);
			} else {
				ball.collisionMove(collisionFound.getCrashPoint(), collisionFound.getVelocityAfter());
			}

		}
		return firstEvent;

	}
	/**
	 * Method for finding balls that might collied with others
	 * @param timeLeft
	 * @return
	 */
	//TODO change so that this class returns a List of pair of balls that might crash, this will make it more efficent at least when having many balls
	private List<Ball> possibleBallCollisions(double timeLeft) {
		List<Ball> ballsClose = new ArrayList<Ball>();
		for(int i = 0; i < balls.size();i++) {
			Ball ball1 = balls.get(i);
			double r1 = ball1.getRadius()+ball1.getVelocity().length()*timeLeft;
			for(int j = i+1; j<balls.size();j++) {
				Ball ball2 = balls.get(j);
				double r2  = ball2.getRadius()+ball2.getVelocity().length()*timeLeft;
				double distanceSQ = ball1.getPosition().distanceSQ(ball2.getPosition());
				if((r1+r2)*(r1+r2)>distanceSQ) {
					if(!ballsClose.contains(ball1)) {
						ballsClose.add(ball1);
					}
					if(!ballsClose.contains(ball2)) {
						ballsClose.add(ball2);
					}
				}
				
			}
		}
		return ballsClose;
	}

	private List<BallCollision> getPolygonCollision(List<? extends Polygon> polygons, double timeLeft) {
		List<BallCollision> collisions = new ArrayList<BallCollision>();
		for (Ball ball : balls) {
			for (Polygon polygon : polygons) {
				List<Point2Double> points = polygon.getGlobalPoints();
				for (int i = 0; i < points.size(); i++) {
					Point2Double point1 = points.get(i);
					Point2Double point2 = points.get((i + 1) % points.size());
					Point2Double normalWall = polygon.getNormalLines().get(i).getVector();
					boolean valueEqualZero = false;

					// When will front point of circle though wall

					Point2Double closestPoint = Point2Double.add(ball.getPosition(),
							normalWall.scalar(-1 * ball.getRadius()));
					IntersectLines intersect = null;
					if (closestPoint != null) {
						intersect = IntersectLines.makeIntersectLines(closestPoint, ball.getVelocity(), point1,
								Point2Double.vector(point2, point1));
					}
					if (intersect != null) {
						if (intersect.getScalar1() < 1e-12 && intersect.getScalar1() > 0) {
							valueEqualZero = true;
							/*
							 * This is needed because error in floating numbers.
							 * Using BigDecimal would increase precision but
							 * would not remove the problem. It would not only
							 * require rewriting of much code. It would also
							 * require implementation of code for things that
							 * are used in Math like sqrt, cos and sin and
							 * whatever else is in use
							 */
							// This value is so small it is effectively the same
							// as
							// zero with accuracy of double
						}
						if (intersect.getScalar1() > 0 && intersect.getScalar2() > 0
								&& intersect.getScalar1() <= timeLeft && intersect.getScalar2() <= 1) {

							// V1after = 2 *(v1 dot N)*N-v1
							Point2Double normal = normalWall.scalar(1 / normalWall.length());
							Point2Double v1after = Point2Double.add(
									normal.scalar(2 * Point2Double.dotProduct(ball.getVelocity(), normal)),
									ball.getVelocity().scalar(-1));
							if (!valueEqualZero) {
								BallCollision move1 = new BallCollision(ball, intersect.getScalar1(), Point2Double
										.add(ball.getPosition(), ball.getVelocity().scalar(intersect.getScalar1())),
										v1after);
								collisions.add(move1);
							}
						}
					}
				}

			}
		}
		return collisions;
	}

	private List<BallCollision> getCornerCollision(List<? extends Polygon> polygons, double timeLeft) {

		List<BallCollision> collisions = new ArrayList<BallCollision>();
		for (Ball ball : balls) {
			for (Polygon polygon : polygons) {
				for (Vector2Double corner : polygon.getNormalPoints()) {
					double distanceNextSQ = Point2Double.add(ball.getPosition(), ball.getVelocity().scalar(timeLeft))
							.distanceSQ(corner.getPoint1());
					double radiusSQ = ball.getRadius() * ball.getRadius();

					if (distanceNextSQ < radiusSQ) {
						// There is a crash with a corner
						// Need to find out what happens
						Point2Double vector = Point2Double.vector(corner.getPoint1(), ball.getPosition());
						Point2Double v1 = new Point2Double(ball.getVelocity());
						// vector is vector from shape to corner before move
						// When collision happens is when | vector + v1*n | = r
						// n^2*dot(v1,v1)+n*2*dot(v1,vector)+dot(vector,vector)-r^2=0
						// This is ax^2+bx+c=0

						double a = Point2Double.dotProduct(v1, v1);
						double b = 2 * Point2Double.dotProduct(v1, vector);
						double c = Point2Double.dotProduct(vector, vector) - radiusSQ;
						// result are n1 and n2
						double n1 = (-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a);
						double n2 = (-b - Math.sqrt(b * b - 4 * a * c)) / (2 * a);
						double crashtime = -1;
						if (n1 > 0 && n1 <= timeLeft && n2 > 0 && n2 <= timeLeft) {
							// Do not think this should happen
							if (n1 < n2) {
								crashtime = n1;
							} else {
								crashtime = n2;
							}
						} else if (n1 > 0 && n1 <= timeLeft) {
							crashtime = n1;
						} else if (n2 > 0 && n2 <= timeLeft) {
							crashtime = n2;
						} else {
							// Should not happen
							return null;
						}

						Point2Double crashPoint = Point2Double.add(ball.getPosition(), v1.scalar(crashtime));
						Point2Double cornerPoint = corner.getPoint1();
						Point2Double normalVector = Point2Double.vector(crashPoint, cornerPoint);
						normalVector = normalVector.scalar(1 / normalVector.length());
						// Point2Double tagentVector = new
						// Point2Double(-normalVector.y, normalVector.x);
						// v1 = v1BeforeTangent + v1BeforeNormal
						Point2Double v1BeforeNormal = normalVector.scalar(Point2Double.dotProduct(normalVector, v1));
						Point2Double v1BeforeTangent = Point2Double.add(v1, v1BeforeNormal.scalar(-1));
						Point2Double v1AfterTagent = v1BeforeTangent;
						Point2Double v1AfterNormal = v1BeforeNormal.scalar(-1);
						Point2Double v1After = Point2Double.add(v1AfterTagent, v1AfterNormal);

						BallCollision move1 = new BallCollision(ball, crashtime, crashPoint, v1After);
						collisions.add(move1);

					}
				}
			}
		}
		return collisions;

	}

	/**
	 * get all collisions between shapes
	 * 
	 * @param timeLeft
	 * @return
	 */
	private List<BallCollision> getBallCollisions(List<Ball> ballsToControl,double timeLeft) {
		List<BallCollision> movements = new ArrayList<BallCollision>();
		for (int i = 0; i < ballsToControl.size(); i++) {
			Ball shape1 = ballsToControl.get(i);
			for (int j = i + 1; j < ballsToControl.size(); j++) {
				Ball shape2 = ballsToControl.get(j);
				boolean foundBefore = false;
				// find out if we have already found out that these 2
				// circles crashes
				for (BallCollision circleCollision : movements) {
					// No need to if instanceof BallToBallCollision check since
					// all here will be that class
					if (circleCollision.getBall().equals(shape1)
							&& ((BallToBallCollision) circleCollision).getCollidesWith().equals(shape2)) {
						foundBefore = true;
						break;
					}
					if (circleCollision.getBall().equals(shape2)
							&& ((BallToBallCollision) circleCollision).getCollidesWith().equals(shape1)) {
						foundBefore = true;
						break;
					}
				}
				if (!foundBefore) {
					BallToBallCollision move1 = resolveBallCrash(shape1, shape2, timeLeft);
					if (move1 != null) {
						if (move1.getTime() > 0 && move1.getTime() < timeLeft) {
							movements.add(move1);
							movements.add(move1.collidesWith);

						}
					}

				}

			}
		}

		return movements;
	}

	private BallToBallCollision resolveBallCrash(Ball ball1, Ball ball2, double timeLeft) {
		Point2Double p1 = ball1.getPosition();
		Point2Double v1 = ball1.getVelocity();
		Point2Double p2 = ball2.getPosition();
		Point2Double v2 = ball2.getVelocity();

		Point2Double p1next = Point2Double.add(p1, v1);
		Point2Double p2next = Point2Double.add(p2, v2);

		double distanceSQ = p1.distanceSQ(p2);
		double distanceSQnext = p1next.distanceSQ(p2next);

		// What will distance be
		double distanceChange = Math.sqrt(distanceSQ) - Math.sqrt(distanceSQnext);
		//Positive value they move towards each other
		//Negative value they move away from each other
		//Zero they are parallel and will never crash at current heading
		
		// We might have a crash if they get close enough
		double r1 = ball1.getRadius();
		double r2 = ball2.getRadius();
		Point2Double vP1P2 = Point2Double.vector(p1, p2);

		Point2Double p1Close = Point2Double.add(p1, vP1P2.scalar(r1 / vP1P2.length()));
		Point2Double p2Close = Point2Double.add(p2, vP1P2.scalar(-r2 / vP1P2.length()));
		// p1Close and p2Close is the 2 points on circles 1 and
		// 2 that have
		// least distance and will be the points that crash
		double distanceNow = Math.sqrt(p1Close.distanceSQ(p2Close));
		double timeToCrash = distanceNow / distanceChange;
		if (timeToCrash > timeLeft || timeToCrash < 0) {
			return null;
			// These 2 circles do not crash at this point
		}

		Point2Double p1Crash;
		Point2Double p2Crash;
		p1Crash = Point2Double.add(p1, v1.scalar(timeToCrash));
		p2Crash = Point2Double.add(p2, v2.scalar(timeToCrash));

		Point2Double normal = Point2Double.vector(p1Crash, p2Crash);
		Point2Double unitNormalVector = normal.scalar(1 / normal.length());
		Point2Double unitTagentVector = new Point2Double(-unitNormalVector.getY(), unitNormalVector.getX());
		/*
		 * Need to find what part of v1 and v2 goes along normal and tagent
		 * vector v1 = v1Normal + v1Tagent v2 = v2Normal + v2Tagent
		 * 
		 */
		double dot1N = Point2Double.dotProduct(unitNormalVector, v1);
		double dot2N = Point2Double.dotProduct(unitNormalVector, v2);
		double dot1T = Point2Double.dotProduct(unitTagentVector, v1);
		double dot2T = Point2Double.dotProduct(unitTagentVector, v2);

		Point2Double v1Normal = unitNormalVector.scalar(dot1N);
		Point2Double v1Tagent = unitTagentVector.scalar(dot1T);
		Point2Double v2Normal = unitNormalVector.scalar(dot2N);
		Point2Double v2Tagent = unitTagentVector.scalar(dot2T);

		double mass1 = ball1.getMass();
		double mass2 = ball2.getMass();
		// Could multiply with PI but that gets canceled out it
		// formula so no need
		// m1v1 + m2v2 = m1v1A + m2v2A
		// 1/2m1v1^2+ 1/2m2v2^2 = 1/2m1v1A^2+ 1/2m2v2A^2

		Point2Double v1Normal2;
		Point2Double v2Normal2;
		double scalar1v1;
		double scalar2v1;
		double scalar1v2;
		double scalar2v2;
		if (mass1 == mass2) {
			scalar1v1 = 0;
			scalar2v1 = 1;
			scalar1v2 = 0;
			scalar2v2 = 1;
		} else {
			scalar1v1 = (mass1 - mass2) / (mass1 + mass2);
			scalar2v1 = 2 * mass2 / (mass1 + mass2);
			scalar1v2 = (mass2 - mass1) / (mass1 + mass2);
			scalar2v2 = 2 * mass1 / (mass1 + mass2);
		}
		v1Normal2 = Point2Double.add(v1Normal.scalar(scalar1v1), v2Normal.scalar(scalar2v1));
		v2Normal2 = Point2Double.add(v2Normal.scalar(scalar1v2), v1Normal.scalar(scalar2v2));

		Point2Double v1After = Point2Double.add(v1Normal2, v1Tagent);
		Point2Double v2After = Point2Double.add(v2Normal2, v2Tagent);

		BallToBallCollision move1 = new BallToBallCollision(ball1, timeToCrash, p1Crash, v1After);
		BallToBallCollision move2 = new BallToBallCollision(ball2, timeToCrash, p2Crash, v2After);
		
		
		move1.setCollidesWith(move2);
		move2.setCollidesWith(move1);
		return move1;
	}

	/**
	 * Returns true if any part of a polygon overlap the other
	 * 
	 * @param polygon1
	 * @param polygon2
	 * @return
	 */
	private boolean polygonIsInsidePolygon(Polygon polygon1, Polygon polygon2) {
		// If any lines intersects polygon intersects
		List<Point2Double> points1 = polygon1.getGlobalPoints();
		List<Point2Double> points2 = polygon2.getGlobalPoints();
		for (int i = 0; i < points1.size(); i++) {
			Point2Double p1 = points1.get(i);
			Point2Double v1 = Point2Double.vector(p1, points1.get((i + 1) % points1.size()));
			for (int j = 0; j < points2.size(); j++) {
				Point2Double p2 = points2.get(j);
				Point2Double v2 = Point2Double.vector(p2, points2.get((j + 1) % points2.size()));
				IntersectLines intersect = IntersectLines.makeIntersectLines(p1, v1, p2, v2);
				if (intersect != null) {
					if (intersect.getScalar1() >= 0 && intersect.getScalar1() <= 1 && intersect.getScalar2() >= 0
							&& intersect.getScalar2() <= 1) {
						return true;
					}
				}
			}
		}

		// Since no line intersections, then is either polygon is completely
		// inside the other
		if (pointIsInsidePolygon(polygon1, polygon2.getGlobalPoints().get(0))
				|| pointIsInsidePolygon(polygon2, polygon1.getGlobalPoints().get(0))) {
			return true;
		}
		return false;
	}

	/**
	 * A vector starting in point intersects polygon n times if n%2 == 0 it is
	 * outside if not it is inside
	 * 
	 * @param polygon
	 * @param point
	 * @return
	 */
	public boolean pointIsInsidePolygon(Polygon polygon, Point2Double point) {
		int intersections = 0;
		List<Point2Double> points = polygon.getGlobalPoints();
		for (int i = 0; i < points.size(); i++) {
			IntersectLines intersect = IntersectLines.makeIntersectLines(point, new Point2Double(1, 0), points.get(i),
					Point2Double.vector(points.get((i + 1) % points.size()), points.get(i)));
			if (intersect != null) {
				if (intersect.getScalar2() >= 0 && intersect.getScalar2() < 1 && intersect.getScalar1() > 0) {
					/*
					 * scalar2 need to be between 0 and 1 scalar1 need to be
					 * positive
					 */
					intersections++;
				}
			}
		}

		if (intersections % 2 == 0) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * Find out if a point with radius is at least radius from edge of polygon
	 * Might get true if a point is inside if all parts of circle is inside
	 * 
	 * @param polygon
	 * @param point
	 * @param radius
	 * @return
	 */
	public boolean doIntersect(Polygon polygon, Point2Double point, double radius) {
		List<Point2Double> points = polygon.getGlobalPoints();
		for (int i = 0; i < points.size(); i++) {
			double distanceSQ = point
					.distanceSQ(closestPoint(points.get(i), points.get((i + 1) % points.size()), point));
			if (distanceSQ < radius * radius) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns closest point to line segment from p1 to p2 to from a point It
	 * will either be p1 or a point in between
	 * 
	 * @param p1
	 * @param p2
	 * @param point
	 * @return
	 */
	public Point2Double closestPoint(Point2Double p1, Point2Double p2, Point2Double point) {
		Point2Double vectorWall = Point2Double.vector(p1, p2);
		Point2Double vector = Point2Double.vector(p1, point);
		double wallSQ = vectorWall.distanceSQ();
		double dotProd = Point2Double.dotProduct(vector, vectorWall);
		double scalar = dotProd / wallSQ;
		if (scalar < 0) {
			return p1;
		}
		if (scalar > 1) {
			return p2;
		}
		return Point2Double.add(p1, vectorWall.scalar(scalar));

	}

	public class BallCollision {

		private final Ball ball;
		private final double time;
		private final Point2Double crashPoint;

		private final Point2Double velocityAfter;
		
	
		public BallCollision(Ball ball, double time, Point2Double crashPoint, Point2Double velocityAfter) {
			this.ball = ball;
			this.time = time;
			this.crashPoint = crashPoint;
			this.velocityAfter = velocityAfter;
		}

		public Ball getBall() {
			return ball;
		}

		public double getTime() {
			return time;
		}

		public Point2Double getCrashPoint() {
			return crashPoint;
		}

		public Point2Double getVelocityAfter() {
			return velocityAfter;
		}

	}

	public class BallToBallCollision extends BallCollision {

		private BallToBallCollision collidesWith;

		public BallToBallCollision(Ball ball, double time, Point2Double crashPoint, Point2Double velocityAfter) {
			super(ball, time, crashPoint, velocityAfter);
		}

		public BallToBallCollision getCollidesWith() {
			return collidesWith;
		}

		public void setCollidesWith(BallToBallCollision collidesWith) {
			this.collidesWith = collidesWith;
		}

	}

}
