package pong;

public class Vector2Double {
	
	private final Point2Double point1;
	private final Point2Double point2;
	private final Point2Double vector;
	
	public Vector2Double(Point2Double point1, Point2Double point2) {
		this.point1 = point1;
		this.point2 = point2;
		vector = Point2Double.vector(point1, point2);
	}
	public Point2Double getPoint1() {
		return point1;
	}
	public Point2Double getPoint2() {
		return point2;
	}
	public Vector2Double moveVector(Point2Double point) {
		return new Vector2Double(Point2Double.add(point1, point),Point2Double.add(point2, point));
	}
	public Point2Double getVector() {
		return vector;
	}
	

}
