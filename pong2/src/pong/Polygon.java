package pong;

import java.util.List;

public interface Polygon {
	
	public List<Point2Double> getGlobalPoints();
	public List<Vector2Double> getNormalLines();
	public List<Vector2Double> getNormalPoints();


}
