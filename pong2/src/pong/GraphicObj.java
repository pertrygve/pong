package pong;

import java.awt.Color;

public interface GraphicObj {
	
	public static final int NORMAL_LENGTH = 20;
	
	public Color getFillColor();
	public Color getEdgeColor();

	public int[][] getAll() ;
	public int[][] getNormalArray();

}
