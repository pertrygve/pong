package pong;

import java.awt.Color;

public class Ball implements GraphicObj {

	public static final int pointsForDraw = 20;

	private Point2Double position;
	private Point2Double velocity;
	private final double radius;
	private final Color fillColor;
	private final Color edgeColor;

	public Ball(Point2Double position, Point2Double velocity, double radius, Color fillColor, Color edgeColor) {
		this.position = position;
		this.velocity = velocity;
		this.radius = radius;
		this.fillColor = fillColor;
		this.edgeColor = edgeColor;
	}

	public Point2Double getPosition() {
		return position;
	}

	public Point2Double getVelocity() {
		return velocity;
	}


	public void movePartial(double time) {
		position = Point2Double.add(position, velocity.scalar(time));

	}

	/**
	 * When a ball collides with something both position and velocity changes
	 * 
	 * @param position
	 * @param velocity
	 */
	public void collisionMove(Point2Double position, Point2Double velocity) {
		this.position = position;
		this.velocity = velocity;
	}

	public double getRadius() {
		return radius;
	}

	@Override
	public Color getFillColor() {
		return fillColor;
	}

	@Override
	public Color getEdgeColor() {
		return edgeColor;
	}

	// If one wants to have a density that could be added so that a heavier ball
	// will have more momentum
	public double getMass() {
		return radius * radius * Math.PI;
	}

	@Override
	public int[][] getAll() {
		int[][] array = new int[2][];
		array[0] = new int[pointsForDraw];
		array[1] = new int[pointsForDraw];
		for (int i = 0; i < pointsForDraw; i++) {
			array[0][i] = (int) (position.getX()
					+ radius * Math.cos(Math.toRadians((double) ((double) i / (double) pointsForDraw) * 360)));
			array[1][i] = (int) (position.getY()
					+ radius * Math.sin(Math.toRadians((double) ((double) i / (double) pointsForDraw) * 360)));

		}
		return array;
	}

	public int[][] getAll(double radiusFactor) {
		int[][] array = new int[2][];
		array[0] = new int[pointsForDraw];
		array[1] = new int[pointsForDraw];
		for (int i = 0; i < pointsForDraw; i++) {
			array[0][i] = (int) (position.getX() + radius * radiusFactor
					* Math.cos(Math.toRadians((double) ((double) i / (double) pointsForDraw) * 360)));
			array[1][i] = (int) (position.getY() + radius * radiusFactor
					* Math.sin(Math.toRadians((double) ((double) i / (double) pointsForDraw) * 360)));

		}
		return array;
	}

	@Override
	public int[][] getNormalArray() {
		int[][] array = new int[4][];
		for (int i = 0; i < array.length; i++) {
			array[i] = new int[1];

		}
		array[0][0] = (int) position.getX();
		array[1][0] = (int) position.getY();
		array[2][0] = (int) (position.getX() + velocity.getX() * NORMAL_LENGTH);
		array[3][0] = (int) (position.getY() + velocity.getY() * NORMAL_LENGTH);
		return array;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Ball) {
			Ball other = (Ball) obj;
			if (!position.equals(other.position)) {
				return false;
			}
			if (!velocity.equals(other.velocity)) {
				return false;
			}
			if (radius != other.radius) {
				return false;
			}
			if (!fillColor.equals(other.fillColor)) {
				return false;
			}
			if (!edgeColor.equals(other.edgeColor)) {
				return false;
			}
			return true;
		}
		return false;
	}

}
