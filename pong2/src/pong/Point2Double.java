package pong;


public class Point2Double {
	
	private final double x;
	private final double y;
	
	public Point2Double(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Point2Double(Point2Double point) {
		x = point.x;
		y = point.y;
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Point2Double) {
			Point2Double other = (Point2Double) obj;
			if(Math.abs(x-other.x) <1e-14 &&Math.abs(y-other.y)<1e-14) {
				return true;
				//Can be the same even if it is because it is no way to avoid rounding errors
			}
		}
		return false;
	}

	public double getX() {
		return x;
	}



	public double getY() {
		return y;
	}



	public static Point2Double add(Point2Double a, Point2Double b) {
		return new Point2Double(a.x+b.x, a.y+b.y);
	}
	/**
	 * vector from v1 to v2
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static Point2Double vector(Point2Double v1, Point2Double v2) {
		return new Point2Double(v2.x-v1.x,v2.y-v1.y);
	}
	
	/**
	 * returns dot product of 2 vectors
	 * 
	 * @param v1
	 * @param v2
	 * @return
	 */

	public static double dotProduct(Point2Double v1, Point2Double v2) {
		return v1.x * v2.x + v1.y * v2.y;
	}

	
	public Point2Double scalar(double scalar) {
		return new Point2Double(x*scalar,y*scalar);
	}
	public double distanceSQ(Point2Double point) {
		return (x-point.x)*(x-point.x)+(y-point.y)*(y-point.y);
	}
	public double distanceSQ() {
		return x*x+y*y;
	}
	
	public double length() {
		return Math.sqrt(distanceSQ());
	}



	@Override
	public String toString() {
		
		return "X "+x+" Y " +y;
	}
	
	

}
