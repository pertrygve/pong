package pong;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Paddle implements GraphicObj, Polygon {

	private Point2Double position;
	private Point2Double velocity;
	private List<Point2Double> points;
	private final Color fillColor;
	private final Color edgeColor;
	private List<Vector2Double> normalLines;
	private List<Vector2Double> normalPoints;

	public Paddle(Point2Double position, Point2Double velocity, List<Point2Double> points, Color fillColor,
			Color edgeColor) {
		this.position = position;
		this.velocity = velocity;
		this.points = points;
		this.fillColor = fillColor;
		this.edgeColor = edgeColor;
		normalLines = new ArrayList<Vector2Double>();
		normalPoints = new ArrayList<Vector2Double>();
		Point2Double firstNormal = new Point2Double(0, 0);
		Point2Double prevNormal = new Point2Double(0, 0);
		for (int i = 0; i < points.size(); i++) {
			Point2Double p1 = points.get(i);
			Point2Double p2 = points.get((i + 1) % points.size());
			Point2Double pCenter = new Point2Double((p1.getX() + p2.getX()) / 2, (p1.getY() + p2.getY()) / 2);
			Point2Double vector = new Point2Double(p2.getX() - p1.getX(), p2.getY() - p1.getY());
			double vectorLenth = Math.sqrt(vector.distanceSQ(new Point2Double(0, 0)));
			Point2Double normal = new Point2Double(vector.getY() * NORMAL_LENGTH / vectorLenth,
					-vector.getX() * NORMAL_LENGTH / vectorLenth);
			normalLines.add(new Vector2Double(pCenter, Point2Double.add(pCenter, normal)));

			if (i == 0) {
				firstNormal = normal;
			} else {
				Point2Double pointNormal = new Point2Double((normal.getX() + prevNormal.getX()) / 2,
						(normal.getY() + prevNormal.getY()) / 2);
				normalPoints.add(new Vector2Double(p1, Point2Double.add(p1, pointNormal)));
			}
			prevNormal = normal;
			if (i == points.size() - 1) {
				Point2Double pointNormal = new Point2Double((normal.getX() + firstNormal.getX()) / 2,
						(normal.getY() + firstNormal.getY()) / 2);
				normalPoints.add(new Vector2Double(p2, Point2Double.add(p2, pointNormal)));

			}
		}
	}

	@Override
	public Color getFillColor() {
		return fillColor;
	}

	@Override
	public Color getEdgeColor() {
		return edgeColor;
	}

	/**
	 * Simple method for moving paddle
	 */
	public void movePaddle() {
		position = Point2Double.add(position, velocity);
		if (position.getX() > Game.sizeX || position.getX() < 0) {
			velocity = new Point2Double(-velocity.getX(), velocity.getY());
		}
		if (position.getY() > Game.sizeY || position.getY() < 0) {
			velocity = new Point2Double(velocity.getX(), -velocity.getY());
		}
	}

	public List<Point2Double> getPoints() {
		return points;
	}

	@Override
	public List<Point2Double> getGlobalPoints() {
		List<Point2Double> globalPoints = new ArrayList<Point2Double>();
		for (Point2Double point : points) {
			globalPoints.add(Point2Double.add(point, position));
		}
		return globalPoints;
	}

	@Override
	public int[][] getAll() {
		int[][] array = new int[2][];
		array[0] = new int[points.size()];
		array[1] = new int[points.size()];
		for (int i = 0; i < points.size(); i++) {
			array[0][i] = (int) (position.getX() + points.get(i).getX());
			array[1][i] = (int) (position.getY() + points.get(i).getY());
		}
		return array;
	}

	@Override
	public int[][] getNormalArray() {
		int[][] array = new int[4][];
		for (int i = 0; i < array.length; i++) {
			array[i] = new int[normalLines.size() + normalPoints.size()];
		}
		for (int i = 0; i < normalLines.size(); i++) {
			array[0][i] = (int) (normalLines.get(i).getPoint1().getX() + position.getX());
			array[1][i] = (int) (normalLines.get(i).getPoint1().getY() + position.getY());
			array[2][i] = (int) (normalLines.get(i).getPoint2().getX() + position.getX());
			array[3][i] = (int) (normalLines.get(i).getPoint2().getY() + position.getY());
		}
		for (int i = 0; i < normalPoints.size(); i++) {
			array[0][i + normalLines.size()] = (int) (normalPoints.get(i).getPoint1().getX() + position.getX());
			array[1][i + normalLines.size()] = (int) (normalPoints.get(i).getPoint1().getY() + position.getY());
			array[2][i + normalLines.size()] = (int) (normalPoints.get(i).getPoint2().getX() + position.getX());
			array[3][i + normalLines.size()] = (int) (normalPoints.get(i).getPoint2().getY() + position.getY());
		}
		return array;

	}

	@Override
	public List<Vector2Double> getNormalLines() {
		return normalLines;
	}

	@Override
	public List<Vector2Double> getNormalPoints() {
		return normalPoints;
	}

}
